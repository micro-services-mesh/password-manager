# Required dependance

- React
- Redux toolkit
- Caddy web server
- Yarn

# How to run project
Install dependency using yarn.
## yarn install

Start react application using following command.
## yarn start

Rename the `Caddyfile.example.dist` to `Caddyfile` and run following command in new terminal.
## caddy start


# Setup SSL certificate 
