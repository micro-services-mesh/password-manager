import React, { PropsWithChildren, Suspense } from 'react';

export const BaseUI = (props: PropsWithChildren<unknown>): JSX.Element => {
	const { children } = props;
	return <Suspense fallback={'loading....'}>{children}</Suspense>;
};

export default BaseUI;
