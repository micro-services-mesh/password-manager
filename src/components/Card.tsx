import { Button, ButtonProps, Card, CardProps, H2 } from '@blueprintjs/core';
import React, { FC } from 'react';

export interface ReactButtonProps extends ButtonProps {
  text?: string;
}

export interface ReactCardFooter {
  style?: {};
  classes?: string;
  buttons?: ReactButtonProps[];
}
export interface ReactCardProps extends CardProps {
  title: string;
  body?: React.ReactNode;
  footer?: ReactCardFooter;
}

const CardItem: FC<ReactCardProps> = (props: ReactCardProps) => {
  const {
    title = '',
    body = undefined,
    footer = undefined,
    ...options
  } = props;

  return (
    <Card {...options}>
      <H2>{title}</H2>
      {props.children}
      <div style={footer?.style} className={footer?.classes}>
        {footer?.buttons?.map((item, index) => {
          return <Button key={index} {...item}></Button>;
        })}
      </div>
    </Card>
  );
};

export default CardItem;
