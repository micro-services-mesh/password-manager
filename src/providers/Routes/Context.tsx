import React, { createContext } from 'react';

import { RouteObject } from 'react-router-dom';

export interface IRouteObject extends RouteObject {
  breadcrumb?: React.ComponentType | React.ElementType | string | null;
  children?: IRouteObject[];
}

export interface IContext {
  routes: IRouteObject[];
  basename?: string;
}

export const defaultState: IContext = {
  routes: [],
  basename: ''
};

export const Context = createContext<IContext>(defaultState);

export default Context;
