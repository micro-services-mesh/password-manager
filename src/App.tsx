import React from 'react';
import { Provider as RoutesProvider, useRoutes } from './providers/Routes';
import router from './app/routers';
import { HashRouter as Router } from 'react-router-dom';

const Loading = () => <div>loading...</div>;
function Routes(): JSX.Element {
  const element = useRoutes();
  console.log(element);
  return <>{element}</>;
}
function App() {
  return (
    <div className='App bp4-dark'>
      <RoutesProvider routes={router}>
        <Router>
          <Routes></Routes>
        </Router>
      </RoutesProvider>
    </div>
  );
}

export default App;
