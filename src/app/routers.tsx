import React, { Children, lazy } from 'react';
import { Outlet } from 'react-router-dom';
import BaseUI from '../containers/BaseUI/BaseUI';
import { IRouteObject } from '../providers/Routes/Context';

const Passwords = lazy(() => import('../features/passwords'));
const Apps = lazy(() => import('../features/apps'));
const Banks = lazy(() => import('../features/bank'));

const routes: IRouteObject[] = [
  {
    path: '/',
    element: (
      <BaseUI>
        <Outlet />
      </BaseUI>
    ),
    children: [
      {
        path: '/',
        breadcrumb: 'root',
        element: <Passwords />,
        children: [
          {
            path: 'apps',
            element: <Apps></Apps>
          },
          {
            path: 'banks',
            element: <Banks />
          }
        ]
      }
    ]
  }
];

export default routes;
