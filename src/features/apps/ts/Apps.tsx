import {
  Button,
  ButtonGroup,
  Classes,
  Dialog,
  Elevation,
  FormGroup,
  InputGroup,
  Tag
} from '@blueprintjs/core';
import React, { useEffect, useMemo, useState } from 'react';
import CardItem from '../../../components/Card';
import { Tooltip2 } from '@blueprintjs/popover2';

const useDialog = () => {
  const DialogComponent = (props: any) => (
    <Dialog
      isOpen={props.open}
      className={Classes.DARK}
      onClose={props.handleOpen}
      title={'sdddd asdas asdas'}
    >
      <div className={Classes.DIALOG_BODY}>
        <FormGroup labelFor='text-input'>
          <InputGroup
            id='text-input'
            placeholder='Application name (required)'
            name='app'
            onChange={(e) => console.log(e.currentTarget.name)}
          />
        </FormGroup>
        <FormGroup labelFor='text-input'>
          <InputGroup id='text-input' placeholder='Member name (required)' />
        </FormGroup>
        <FormGroup labelFor='text-input'>
          <InputGroup id='text-input' placeholder='Tags (required)' />
        </FormGroup>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Tooltip2 content='This button is hooked up to close the dialog.'>
            <Button intent='primary' onClick={props.onClickSave}>
              Save
            </Button>
          </Tooltip2>
          <Tooltip2 content='Cancel action' position='top'>
            <Button onClick={props.handleOpen}>Cancel</Button>
          </Tooltip2>
        </div>
      </div>
    </Dialog>
  );
  return { DialogComponent };
};

const Apps = () => {
  const [items, setItems] = useState<any>([]);
  const [open, setOpen] = useState<boolean | undefined>(false);
  const handleOpen = () => {
    setOpen(!open);
  };

  useEffect(() => {
    setItems([
      {
        name: 'YouTube',
        member_name: 'snehaldangroshiya@gmail.com',
        tags: 'Application;youtube'
      },
      {
        name: 'Google',
        member_name: 's.dangroshiya@kopano.com',
        tags: 'Application'
      },
      {
        name: 'Kotak Bank',
        member_name: 'dharadangroshiya@gmail.com',
        tags: 'Bank'
      }
    ]);
  }, []);

  const onClickSave = () => {
    handleOpen();
  };

  const { DialogComponent } = useDialog();

  return (
    <div style={{ margin: '1rem' }}>
      <ButtonGroup minimal style={{ margin: '0.5rem' }}>
        <Button text='Create' icon='plus' onClick={handleOpen} />
      </ButtonGroup>
      <div
        style={{
          display: 'grid',
          columnGap: '1rem',
          rowGap: '1rem',
          gridTemplateColumns: 'repeat(auto-fill, minmax(500px, 1fr))'
        }}
      >
        {items.map((item: any, index: number) => (
          <CardItem
            key={index}
            footer={{
              buttons: [
                {
                  intent: 'primary',
                  text: 'Open',
                  type: 'button',
                  onClick: handleOpen
                }
              ]
            }}
            elevation={Elevation.TWO}
            title={item.name}
          >
            {item.tags.split(';').map((tag: string, index: number) => (
              <Tag
                key={index}
                style={{ marginRight: '0.3rem' }}
                interactive
                round
              >
                {tag}
              </Tag>
            ))}

            <div style={{ padding: '1rem 0rem 2rem 0rem' }}>
              {item.member_name}
            </div>
          </CardItem>
        ))}
      </div>
      <DialogComponent
        open={open}
        handleOpen={handleOpen}
        onClickSave={onClickSave}
      ></DialogComponent>
    </div>
  );
};

export default Apps;
