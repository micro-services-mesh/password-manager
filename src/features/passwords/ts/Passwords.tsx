import { Alignment, Button, Navbar } from '@blueprintjs/core';
import React from 'react';
import { Outlet, useNavigate } from 'react-router-dom';

const Passwords = (props: any) => {
  const navigate = useNavigate();
  return (
    <>
      <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Password Manager</Navbar.Heading>
          <Navbar.Divider />
          <Button
            className='bp4-minimal'
            icon='mobile-phone'
            text='Apps'
            onClick={() => navigate('/apps')}
          />
          <Button
            className='bp4-minimal'
            icon='office'
            text='Banks'
            onClick={() => navigate('/banks')}
          />
        </Navbar.Group>
      </Navbar>
      <Outlet />
    </>
  );
};
export default Passwords;
